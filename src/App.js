import React from 'react';
import './App.css';
import {MainLayout} from "./components/MainLayout";

const App = () => (
    <div className="App">

        <MainLayout/>
    </div>
);


export default App;
